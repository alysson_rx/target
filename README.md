# target

Target armazena os resultados dos flechas e mostra o resultado em um gráfico.

## Getting Started

Clonar o projeto

```shell script
git clone git@gitlab.com:alysson_rx/target.git
``` 

Get the packages

```shell script
flutter pub get
```

Run tests 

```shell script
flutter test
```