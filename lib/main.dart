import 'package:flutter/material.dart';

void main() {
  runApp(TargetApp());
}

class TargetApp extends StatelessWidget {
  static const String _title = 'Target App';
  static const String keyName = 'target-app';

  const TargetApp({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      key: Key(keyName),
      title: _title,
      home: TargetAppWidget(),
    );
  }
}

class TargetAppWidget extends StatefulWidget {
  @override
  _TargetAppWidgetState createState() => _TargetAppWidgetState();
}

class _TargetAppWidgetState extends State<TargetAppWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('My app'),
        ],
      ),
    );
  }
}
